=================
aiohttp Redis API
=================

Example application built with ``aiohttp`` and ``aioredis`` thae exposes an API for a
Redis server. The API is composed by:

* ``POST http://127.0.0.1:8000/key/{key}``: with a body of `value=text`, adds a new key
* ``GET http://127.0.0.1:8000/key/{key}``: retrieves the given key

The application is traced using `dd-trace-py`_ library and the time spent in handling
the request and retrieving the key from Redis are captured.

.. _dd-trace-py: https://github.com/DataDog/dd-trace-py

Requirements
------------

Redis and the Datadog agent are required to run this example. If you want to launch both
services using Docker and ``docker-compose``, please install them using the `instructions
provided`_ by your platform.

.. _instructions provided: https://www.docker.com/community-edition

Getting started
---------------

Before you continue, be sure you have an active Datadog account. If you don't have it, you
can `register for a trial`_ account to install and obtain your API key. For more information
please follow `our documentation`_.

.. _register for a trial: https://app.datadoghq.com/signup
.. _our documentation: https://docs.datadoghq.com/

When you've obtained your API key, you can launch Redis and the Datadog Agent with::

    $ DD_API_KEY=<your_api_key> docker-compose up -d

Then you have to launch the ``aiohttp`` application as follows::

    $ cd app
    $ python main.py

You can use ``curl`` to make some requests to set and get a key::

    $ curl -X POST -d'value=is_here' http://127.0.0.1:8000/key/tracing
    $ curl http://127.0.0.1:8000/key/tracing

License
-------

``aiohttp-context-example`` is released under the terms of the **BSD license**. Full details in ``LICENSE`` file.
