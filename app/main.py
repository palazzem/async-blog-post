import os
import asyncio
import aioredis

from aiohttp import web
from routes import setup_routes

from ddtrace import tracer
from ddtrace.contrib.aiohttp import trace_app


async def make_app(loop):
    app = web.Application(loop=loop)

    # get some configurations from our environment
    config = {
        'host': os.getenv('APP_HOST', '127.0.0.1'),
        'port': int(os.getenv('APP_PORT', 8000)),
        'redis_host': os.getenv('REDIS_HOST', '127.0.0.1'),
        'redis_port': int(os.getenv('REDIS_PORT', 6379)),
    }
    app['config'] = config

    # configure a Redis connection pool
    address = (app['config']['redis_host'], app['config']['redis_port'])
    app['redis_pool'] = await aioredis.create_pool(address, maxsize=1, loop=loop)

    # configure the web application
    setup_routes(app)
    return app


if __name__ == '__main__':
    # create the application
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(make_app(loop))

    # add tracing
    tracer.set_tags({'env': 'blog-post'})
    trace_app(app, tracer, service='async-redis-api')

    # start the web application
    web.run_app(
        app,
        host=app['config']['host'],
        port=app['config']['port'],
    )
