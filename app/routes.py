from views import key_lookup, key_set


def setup_routes(app):
    app.router.add_get('/key/{key_id}', key_lookup)
    app.router.add_post('/key/{key_id}', key_set)
