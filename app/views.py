from aiohttp import web

from ddtrace import tracer


@tracer.wrap('redis_pool.get', service='aioredis')
async def redis_get(redis_pool, key):
    with await redis_pool as redis:
        value = await redis.connection.execute('get', key)
        if value:
            return value.decode('utf-8')


@tracer.wrap('redis_pool.set', service='aioredis')
async def redis_set(redis_pool, key, value):
    with await redis_pool as redis:
        result = await redis.connection.execute('set', key, value)
        return result.decode('utf-8')


async def key_lookup(request):
    # retrieve the key and use an available connection
    key_id = request.match_info['key_id']
    pool = request.app['redis_pool']
    value = await redis_get(pool, key_id)

    if value:
        status = 200
        text = value
    else:
        status = 404
        text = 'Key not found'
    return web.Response(text=text, status=status)


async def key_set(request):
    # retrieve the key and use an available connection
    key_id = request.match_info['key_id']
    data = await request.post()
    value = data.get('value')

    if not value:
        return web.Response(text='Bad request', status=400)

    pool = request.app['redis_pool']
    result = await redis_set(pool, key_id, value)
    return web.Response(text=result, status=201)
